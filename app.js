//JSON data
let inventaryRecords = require('./queries.json');
var express = require('express');
var app = express();
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var path = require('path');

app.use(cookieParser());
//app.use(express.static('public'));
//app.use(express.static(path.join(__dirname, 'public')));

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({
		extended: true
}));


var Admincredentials = {
    userName: "username@admin",
    password: "password@admin"
};


 




function BasicAuth(req, res) {
	if (!req.headers.authorization || req.headers.authorization.indexOf('Basic ') === -1) {
		res.status(401).json({ message: 'Missing Authorization Header' });
		return false;
        
    }
    // verify auth credentials
    const base64Credentials =  req.headers.authorization.split(' ')[1];
    const credentials = Buffer.from(base64Credentials, 'base64').toString('ascii');
    const [username, password] = credentials.split(':');
	if (!(username === Admincredentials.userName && password === Admincredentials.password)) {
		
		res.status(401).json({ message: 'Invalid Authentication Credentials'});
		return false
    }
	
    return true;
}



app.use((req, res, next) => {
		res.header("Access-Control-Allow-Origin", "*");
		res.header(
				"Access-Control-Allow-Headers",
				"Origin, X-Requested-With, Content-Type, Accept, Authorization"
		);
		if (req.method === "OPTIONS") {
				res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
				return res.status(200).json({});
		}	
		
		next();
});



const fs = require('fs');
	mongoose.connect(
		"mongodb+srv://sid:kctaneja@cluster0-pqe9f.mongodb.net/test?retryWrites=true&w=majority", {
				useNewUrlParser: true
		}
	).then(() => {
		console.log('mongoose connected');
	},
	err => {
		console.log('mongoose connection failed');
 	}
);



const numberOfSeats = 40
var seatSchema = mongoose.Schema({
		seatNumber: Number,
		status : String,
		userDetails: {
				name: String,
				age: Number,
		},
		updatedAt: {
			type: Date,
			default: Date.now
		}
});

var Seats = mongoose.model("Seats", seatSchema);


const status = {
    OPEN: 'OPEN',
    CLOSED: 'CLOSED'
}
	

/*
app.post("/addname", (req, res) => {
	
		var myData = new User({username: "Maheshv1", password: "Mahesh@12"});
		
		myData.save().then(item => {
				res.send("Name saved to database" + myData);
		}).catch(err => {
				res.status(400).send("Unable to save to database");
		});
});
*/


// serve the homepage
/*
app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});
*/


app.post("/addSeat", (req, res) => {
		
		console.log("addSeat Input Payload " + JSON.stringify(req.body));
		
		
		var Name = ["Mahesh", "Manisha", "Umesh", "Ram", "Om"];
		var Age = [24, 25, 27, 28, 29];
		var posstatus = [status.OPEN, status.CLOSED];
		var i = 0;
		var ListOfTickets = [];	
		for (i = 0; i < numberOfSeats; i++) {
		
			var seat = new Seats ({
				seatNumber: i+1,	
				status: posstatus[i%2],
				userDetails : { 
					name: Name[i%5],
					age: Age[i%5]
				}
			});
			
			if (seat.status == status.OPEN) {
				seat.userDetails = null
			}
				
			ListOfTickets.push(seat);
			
		}	
		console.log(ListOfTickets);

		Seats.insertMany(ListOfTickets, function(error, docs) {
			
			if (error) {
				console.log(error);
				res.status(500).json({error: "Couldn't Save up all the tickets"});
				return;
			}
			console.log(docs.length + "tickets saved to DB");

		});

		res.status(201).json(ListOfTickets);
				
});




app.get('/getAllSeats', function(req, res) {
	console.dir("getAllSeats_Request Paylaod_" + JSON.stringify(req.body));
	Seats.find({}, function(err, seats) {
		if (err) {
			console.log(err);
			res.status(500).json({error: err});
			return;
		} 
		console.log("Fetched all the seats from DB total count is " + seats.length);
		res.status(200).send(seats);
    });
});

// userdetails of all the closed tickets can be not null or null but for OPEN tickets it must be => userDetails = null
app.get('/getUserDetails/:seatNumber', function(req, res) {
	console.dir("getTicketStatus Paylaod_" + JSON.stringify(req.body) + "and Params " + JSON.stringify(req.params));
	var seatNo = req.params.seatNumber
	
	Seats.findOne({ seatNumber: seatNo }, function (err, seat){
		if (err) {
			res.status(500).json({error: err});
			console.log(err);
			return
		} 
		console.log("Fetched Details of Ticket No :" + seatNo);
		if (seat == null) {
			var x  = "Ticket with seatNumber " + seatNo + " not exists in DB"	 
			res.status(500).json({error: x});
			console.log(x);
			return;
		}
		var cur_status = seat.status;	
		if (cur_status == status.OPEN) {
			res.status(400).json({error: "Ticket is OPEN, Hence userDetails not found"});
			console.log("Ticket is OPEN");
			return;
		}
		console.log("Ticket Details " + JSON.stringify(seat));
		// can be given only status.
		res.status(200).json(seat);
    });
});





app.get('/getTicketStatus/:seatNumber', function(req, res) {
	console.dir("getTicketStatus Paylaod_" + JSON.stringify(req.body) + "and Params " + JSON.stringify(req.params));
	var seatNo = req.params.seatNumber
	
	Seats.findOne({ seatNumber: seatNo }, function (err, seat){
		if (err) {
			res.status(500).json({error: err});
			console.log(err);
			return;
		} 
		
		if (seat == null) {
			var x  = "Ticket with seatNumber " + seatNo + " not exists in DB"	 
			res.status(404).json({error: x});
			console.log(x);
			return;
		}
		console.log("Fetched Details of Ticket No :" + seatNo);
		console.log("Ticket Details " + JSON.stringify(seat));
		// can be given only status.
		res.status(200).json(seat);
    });
});


app.put('/openTicket/:seatNumber', function(req, res) {
	console.dir("openTicket Paylaod_" + JSON.stringify(req.body) + "and Params " + JSON.stringify(req.params));
	var seatNo = req.params.seatNumber
	
	Seats.findOne({ seatNumber: seatNo }, function (err, seat){
		if (err) {
			console.log(err);
			res.status(500).json({error: err});
			return;
		
		}
		// also add if findOne doesn't get any seat.
		if (seat == null) {
			var x  = "Ticket with seatNumber " + seatNo + " not exists in DB"	 
			res.status(404).json({error: x});
			console.log(x);
			return;
		}
		var cur_status = seat.status;	
		if (cur_status == status.OPEN) {
			res.status(400).json({error: "Ticket is already OPEN"});
			console.log("Ticket is already OPEN");
			return;
		}
		
		seat.updatedAt = Date.now();
		seat.userDetails = null; // to remove  userDetails of current user.
		seat.status = status.OPEN;
		
		console.log("Ticket Status"  + " " + JSON.stringify(seat));
		
		seat.save().then(item => {
				console.log("Ticket  successfully Opened and saved to DB for seatNo" + seatNo);
				res.status(201).json(seat);
			}).catch(err => {
					console.log(err);
					res.status(400).send("Unable to save open ticket status in DB");
			});	
		
	});

});


app.put('/closeTicket/:seatNumber', function(req, res) {
	console.dir("closeTicket Paylaod_" + JSON.stringify(req.body) + "and Params " + JSON.stringify(req.params));
	var seatNo = req.params.seatNumber
		
	Seats.findOne({ seatNumber: seatNo }, function (err, seat){
		if (err) {
			res.status(500).json({error: err});
			console.log(err);
			return;
		}
		// also add if findOne doesn't get any seat.
		if (seat == null) {
			var x  = "Ticket with seatNumber " + seatNo + " not exists in DB"	 
			res.status(404).json({error: x});
			console.log(x);
			return;
		}
		var cur_status = seat.status;	
		if (cur_status == status.CLOSED) {
			res.status(400).json({error: "Ticket is already CLOSED"});
			console.log("Ticket is already CLOSED");
			return;
		}
		seat.updatedAt = Date.now();
		seat.status = status.CLOSED;
		
		console.log("Ticket Status" + JSON.stringify(seat));
		
		seat.save().then(item => {
				console.log("Ticket  successfully Closed and saved to DB for seatNo : " + seatNo);
				res.status(201).json(seat);
			}).catch(err => {
					res.status(500).send("Unable to save Closed ticket status in DB");
			});	
		
	});

});


app.post('/addUserDetails/:seatNumber', function(req, res) {
	console.dir("addUserDetails Paylaod_" + JSON.stringify(req.body) + "and Params " + JSON.stringify(req.params));
	var seatNo = req.params.seatNumber
	var userDetails = req.body.userDetails;
	
	Seats.findOne({ seatNumber: seatNo }, function (err, seat){
		if (err) {
			res.status(500).json({error: err});
			console.log(err);
			return;
		}
		// also add if findOne doesn't get any seat.
		if (seat == null) {
			var x  = "Ticket with seatNumber " + seatNo + " not exists in DB"	 
			res.status(404).json({error: x});
			console.log(x);
			return;
		}		
		var cur_status = seat.status;	
		if (cur_status == status.OPEN) {
			res.status(400).json({error: "This Ticket is still OPEN, no user associated with this ticket"});
			console.log("Ticket is still OPEN");
			return;
		}
		if (userDetails == null) {
			res.status(400).json({error: "UserDetails are not provided correctly"});
			console.log("UserDetails are not provided correctly");
			return;
		}
		seat.userDetails = userDetails
		seat.updatedAt = Date.now();
		console.log("updated Ticket is " + JSON.stringify(seat));
		seat.save().then(item => {
				console.log("User Details successfully added and saved to DB for seatNo : " + seatNo);
				res.status(201).json(seat);
				//res.send("User Details successfully added and saved to DB");
			}).catch(err => {
					res.status(500).send("Unable to add user Details for a ticket in DB");
			});	
		
	});

});



app.get('/getAllOpenTickets', function(req, res) {
	console.dir("getAllOpenTickets Payload_" + JSON.stringify(req.body));
	Seats.find({ status: status.OPEN }, function(err, seats) {
		if (err) {
			res.status(500).json({error: err});
			console.log(err);
			return;
		} 
		console.log("Fetched all the OPEN tickets from DB total count is " + seats.length);
		if (seats.length == 0) {
			var x  = "No Open Tickets exists in DB";
			res.status(400).json({error: x});
			console.log(x);
			return;
		}
		res.status(200).json(seats);
    });
});

app.get('/getAllClosedTickets', function(req, res) {
	console.dir("getAllClosedTickets Payload_" + JSON.stringify(req.body));
	Seats.find({ status: status.CLOSED }, function(err, seats) {
		if (err) {
			res.status(500).json({error: err});
			console.log(err);
			return;
		} 
		console.log("Fetched all the CLOSED tickets from DB total count is " + seats.length);
		if (seats.length == 0) {
			var x  = "No Closed Tickets exists in DB";
			res.status(400).json({error: x});
			console.log(x);
			return;
		}
		
		res.status(200).json(seats);
    });
});



app.put('/OpenAllTickets', function(req, res) {
	
	
	console.dir("OpenAllTickets Paylaod_" + JSON.stringify(req.body) + "and Params " + JSON.stringify(req.params));
	
	if (!BasicAuth(req, res)) {
		console.log("API Caller is not an admin");
		return; // unauthorized;
	} else {
		console.log("Admin Authenticated");
	}
	
	var bulk = Seats.collection.initializeOrderedBulkOp();
	Seats.updateMany({status: status.CLOSED},{$set: {status: status.OPEN, userDetails: null}},
	function(err, result) {
      if (err) {
		console.log(err);
		res.status(500).json({error: "Couldn't OPEN up all the tickets"});
		return;
      }
	  res.status(200).json({success: "successfully OPENED all the tickets"});
	  console.log("Result : " + JSON.stringify(result));
    });
	console.log("Successfully Updated all the Tickets as OPEN");
	
});




app.delete('/deleteAllSeats', function(req, res) {
	console.dir("deleteAllSeats Payload_" + JSON.stringify(req.body));
	Seats.deleteMany({}, function(err) {
            if (err) {
                res.status(500).json({error: err});
            } else {
                res.status(200).send('successfully deleted all the seats');
            }
        }
    );
});



app.listen(9000, function() {
		console.log('server running at port 9000');
})
